package com.company.android.phonestore;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.widget.ImageView;
import android.widget.TextView;

import com.company.android.phonestore.BestBuyFetcher.Category;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by hics on 1/14/16.
 */
public class CatalogFragment extends Fragment {
    @SuppressWarnings("unused")
    private static final String TAG = CatalogFragment.class.getSimpleName();

    private int mPage;

    private RecyclerView mCatalogView;
    private SwipeRefreshLayout mSwipeContainer;

    private FetchProductsTask mProductsTask;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);

        resetCatalog();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        View v = inflater.inflate(R.layout.fragment_catalog, container, false);

        mSwipeContainer = (SwipeRefreshLayout)
                v.findViewById(R.id.swipe_refresh_container);
        mSwipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                Log.i(TAG, "Refresh!");
                resetCatalog();
            }
        });

        mCatalogView = (RecyclerView) v.findViewById(R.id.catalog);
        mCatalogView.setAdapter(new CatalogAdapter(ProductsBank.self().getProducts()));

        /* Set temporary value for spanCount. It will change later.*/
        mCatalogView.setLayoutManager(new GridLayoutManager(getActivity(), 1));

        /*
            Register a LayoutListener to update number of columns on catalog
            after we know its actual width.
         */
        ViewTreeObserver treeObserver = mCatalogView.getViewTreeObserver();
        treeObserver.addOnGlobalLayoutListener(new OnGlobalLayoutListener()
        {
            private boolean mIsFirstLayoutPass = true;

            @Override
            public void onGlobalLayout() {
                if (mIsFirstLayoutPass) {
                    mIsFirstLayoutPass = false;

                    int maxWidth = mCatalogView.getWidth();

                    DisplayMetrics metrics = new DisplayMetrics();

                    Display display = getActivity().getWindowManager()
                            .getDefaultDisplay();
                    display.getMetrics(metrics);

                    /*
                        Find how many columns catalog's grid should have
                        so every one is at least 1 inch wide.
                     */

                    int columns = (int) Math.floor(maxWidth / metrics.xdpi);

                    //Update columns number.
                    ((GridLayoutManager) mCatalogView
                            .getLayoutManager()).setSpanCount(columns);

                }
            }
        });

        mCatalogView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {

                int childCount = recyclerView.getChildCount();
                View lastChild = recyclerView.getChildAt(childCount - 1);

                if (lastChild != null) {
                    int adapterPosition = recyclerView
                            .getChildAdapterPosition(lastChild);

                    int totalCount = recyclerView.getAdapter().getItemCount();
                    if (adapterPosition + 1 == totalCount) {
                        loadNextProductsPage();
                    }
                }

            }
        });

        return v;
    }

    private void resetCatalog() {
        mPage = 0;
        ProductsBank.self().clear();
        if (mCatalogView != null) {
            Adapter adapter = mCatalogView.getAdapter();
            adapter.notifyDataSetChanged();
        }
        loadNextProductsPage();
    }

    private void loadNextProductsPage() {
        if (mProductsTask == null) {
            // One task at a time!
            mPage += 1;
            mProductsTask = new FetchProductsTask();
            mProductsTask.execute(mPage);
        }
    }

    private class CatalogAdapter extends Adapter<ProductHolder> {

        private final List<Product> mProducts;

        public CatalogAdapter(List<Product> products) {
            mProducts = products;
        }

        @Override
        public ProductHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = getActivity().getLayoutInflater()
                    .inflate(R.layout.catalog_product, parent, false);
            return new ProductHolder(v);
        }

        @Override
        public void onBindViewHolder(ProductHolder holder, int position) {
            holder.bind(mProducts.get(position));
        }

        @Override
        public int getItemCount() {
            return mProducts.size();
        }
    }

    private class ProductHolder extends ViewHolder implements View.OnClickListener {

        private final ImageView mThumbnailView;
        private final TextView mNameView;
        private final TextView mBrandView;
        private final TextView mPriceView;

        private Product mProduct;

        public ProductHolder(View itemView) {
            super(itemView);

            itemView.setOnClickListener(this);

            mThumbnailView = (ImageView) itemView
                    .findViewById(R.id.catalog_product_thumbnail);
            mNameView = (TextView) itemView
                    .findViewById(R.id.catalog_product_name);
            mBrandView = (TextView) itemView
                    .findViewById(R.id.catalog_product_brand);
            mPriceView = (TextView) itemView
                    .findViewById(R.id.catalog_product_price);
        }

        public void bind(Product product) {
            mProduct = product;

            Picasso.with(getActivity())
                    .load(product.getThumbnailUrl())
                    .placeholder(R.drawable.placeholder)
                    .into(mThumbnailView);

            mNameView.setText(product.getName());
            mBrandView.setText(product.getBrand());

            String priceText = getResources()
                    .getString(R.string.catalog_price_template, mProduct.getPrice());
            mPriceView.setText(priceText);
        }

        @Override
        public void onClick(View v) {
            Intent i = new Intent(getActivity(), ProductActivity.class);
            i.putExtra(ProductFragment.PRODUCT_EXTRA, mProduct.getSku());
            startActivity(i);
        }
    }

    private class FetchProductsTask extends AsyncTask<Integer, Void, List<Product>> {

        @Override
        protected List<Product> doInBackground(Integer... params) {
            return new BestBuyFetcher(Category.CELLPHONE).getProducts(params[0]);
        }

        @Override
        protected void onPostExecute(List<Product> products) {

            int oldCount = ProductsBank.self().count();
            int addedCount = products.size();

            ProductsBank.self().add(products);

            Log.i(TAG, "Page " + mPage + ". Products added: " + addedCount);

            if (CatalogFragment.this.isAdded()) {
                Adapter adapter = mCatalogView.getAdapter();
                adapter.notifyItemRangeInserted(oldCount, addedCount);
            }

            if (mSwipeContainer.isRefreshing()) {
                mSwipeContainer.setRefreshing(false);
            }

            mProductsTask = null;
        }
    }

}
