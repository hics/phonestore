package com.company.android.phonestore;

import android.app.Dialog;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.Rect;
import android.graphics.RectF;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

/**
 * Created by hics on 1/19/16.
 */
public class ImageDialog extends DialogFragment {
    @SuppressWarnings("unused")
    private static final String TAG = ImageDialog.class.getSimpleName();

    public static final String IMAGE_EXTRA =
        "com.company.android.phonestore.ImageDialog.IMAGE_EXTRA";

    private Bitmap mImage;

    private boolean mIsDialog;

    public static ImageDialog newInstance(Bitmap bitmap) {
        Bundle bundle = new Bundle();
        bundle.putParcelable(IMAGE_EXTRA, bitmap);

        ImageDialog dialog = new ImageDialog();
        dialog.setArguments(bundle);

        return dialog;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NO_TITLE, 0);

        mImage = getArguments().getParcelable(IMAGE_EXTRA);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        mIsDialog = true;
        return super.onCreateDialog(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {

        ImageView imageView = new ImageView(getActivity());

        if (mIsDialog) {
            /* When dialog, scale image to fit decorWindow area minus dialog padding */

            int[] attrs = {R.attr.dialogPreferredPadding};
            TypedArray typedArray = getActivity()
                    .obtainStyledAttributes(attrs);

            int padding = typedArray.getDimensionPixelSize(0, 0);

            typedArray.recycle();

            Rect decorRect = new Rect();
            getDialog().getWindow().getDecorView()
                    .getWindowVisibleDisplayFrame(decorRect);

            int maxWidth = decorRect.width() - padding;
            int maxHeight = decorRect.height() - padding;

            int imageWidth = mImage.getWidth();
            int imageHeight = mImage.getHeight();

            Matrix m = new Matrix();
            m.setRectToRect(new RectF(0,0,imageWidth,imageHeight),
                    new RectF(0,0,maxWidth,maxHeight),
                    Matrix.ScaleToFit.CENTER);

            mImage = Bitmap.createBitmap(mImage, 0, 0, imageWidth, imageHeight, m, false);

        }

        imageView.setImageBitmap(mImage);

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        return imageView;
    }

}
