package com.company.android.phonestore;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.facebook.share.model.SharePhoto;
import com.facebook.share.model.SharePhotoContent;
import com.facebook.share.widget.ShareButton;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

/**
 * Created by hics on 1/18/16.
 */
public class ProductFragment extends Fragment {
    @SuppressWarnings("unused")
    private static final String TAG = ProductFragment.class.getSimpleName();

    public static final String PRODUCT_EXTRA =
            "com.company.android.phonestore.ProductActivity.PRODUCT_EXTRA";

    private Product mProduct;
    private ImageView mProductImageView;

    public static Fragment newInstance(int sku) {
        Bundle bundle = new Bundle();
        bundle.putInt(PRODUCT_EXTRA, sku);

        Fragment fragment = new ProductFragment();
        fragment.setArguments(bundle);

        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);

        mProduct = ProductsBank.self()
                .findBySku(getArguments().getInt(PRODUCT_EXTRA));
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        View v = inflater.inflate(R.layout.fragment_product, container, false);

        mProductImageView = (ImageView) v
                .findViewById(R.id.product_image);

        final ShareButton shareButton = (ShareButton) v.
                findViewById(R.id.fb_share_button);

        Picasso.with(getActivity())
                .load(mProduct.getImageUrl())
                .placeholder(R.drawable.placeholder)
                .into(mProductImageView, new Callback() {

                    @Override
                    public void onSuccess() {
                        /* Get image to share and configure Facebook button */

                        final Bitmap image = ((BitmapDrawable) mProductImageView
                                .getDrawable()).getBitmap();
                        SharePhoto photo = new SharePhoto.Builder()
                                .setBitmap(image)
                                .build();
                        SharePhotoContent content = new SharePhotoContent.Builder()
                                .addPhoto(photo)
                                .build();

                        shareButton.setShareContent(content);
                        shareButton.setVisibility(View.VISIBLE);

                        /* Add behaviour "click to expand" on image */

                        mProductImageView.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                DialogFragment dialogFragment =
                                        ImageDialog.newInstance(image);
                                dialogFragment.show(getFragmentManager(), "ImageDialog");
                            }
                        });
                    }

                    @Override
                    public void onError() {
                        // Empty
                    }
                });

        TextView nameView = (TextView)
                v.findViewById(R.id.product_name);
        nameView.setText(mProduct.getName());

        TextView descriptionView = (TextView)
                v.findViewById(R.id.product_description);
        descriptionView.setText(mProduct.getDescription());

        String brandText = getResources()
                .getString(R.string.product_brand_template, mProduct.getBrand());
        TextView brandView = (TextView) v
                .findViewById(R.id.product_brand);
        brandView.setText(brandText);

        String priceText = getResources()
                .getString(R.string.product_price_template, mProduct.getPrice());
        TextView priceView = (TextView) v
                .findViewById(R.id.product_price);
        priceView.setText(priceText);

        return v;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        Picasso.with(getActivity())
                .cancelRequest(mProductImageView);
    }
}
