package com.company.android.phonestore;

import android.net.Uri;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by hics on 1/15/16.
 */
public class BestBuyFetcher {
    @SuppressWarnings("unused")
    private static final String TAG = BestBuyFetcher.class.getSimpleName();

    private static final String API_KEY = "4rtje6kyeysvmdynnmhw37ue";
    private static final String PRODUCTS_URL = "https://api.bestbuy.com/v1/products";

    private Uri mUri;

    public BestBuyFetcher(Category category) {
        if (category == null) {
            throw new IllegalArgumentException("Category cannot be null!");
        }

        String url = PRODUCTS_URL + "((categoryPath.id=" + category.getPathId() + "))";

        mUri = Uri.parse(url).buildUpon()
                .appendQueryParameter("apiKey", API_KEY)
                .appendQueryParameter("sort", "startDate.dsc")
                .appendQueryParameter("show", productAttributesToShow())
                .appendQueryParameter("pageSize", "50")
                .appendQueryParameter("format", "json")
                .build();
    }

    public List<Product> getProducts(int page) {
        if (page < 1) {
            throw new IllegalArgumentException("Page must me greater than 0!");
        }

        Uri pagedUri = mUri.buildUpon()
                .appendQueryParameter("page", String.valueOf(page))
                .build();

        List<Product> products = new ArrayList<>();

        try {
            String response = new String(fetchFromUrl(pagedUri.toString()));
            Log.d(TAG, response);

            parseJsonResponse(response, products);

        } catch (IOException e) {
            Log.e(TAG, "Error downloading products!", e);
        } catch (JSONException e) {
            Log.e(TAG, "Error parsing json!", e);
        }

        return products;

    }

    public static byte[] fetchFromUrl(String urlSpec) throws IOException {
        URL url = new URL(urlSpec);
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        try {
            if (connection.getResponseCode() != HttpURLConnection.HTTP_OK) {
                throw new IOException(connection.getResponseMessage() + ": with " + url);
            }

            InputStream inputStream = connection.getInputStream();
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

            int bytesRead = 0;
            byte[] buffer = new byte[1024];
            while ((bytesRead = inputStream.read(buffer)) > 0) {
                outputStream.write(buffer, 0, bytesRead);
            }
            outputStream.close();

            return outputStream.toByteArray();

        } finally {
            connection.disconnect();
        }
    }

    private void parseJsonResponse(String json, List<Product> dstList)
            throws JSONException
    {
        JSONObject rootObject = new JSONObject(json);
        JSONArray productsJsonArray = rootObject.getJSONArray("products");

        int productsNumber = productsJsonArray.length();
        for (int i = 0; i < productsNumber; i++) {
            JSONObject productJson = productsJsonArray.getJSONObject(i);

            Product product = new Product();
            product.setSku(productJson.getInt(ProductAttribute.SKU));
            product.setPrice((float) productJson.getDouble(ProductAttribute.PRICE));
            product.setName(productJson.getString(ProductAttribute.NAME));
            product.setImageUrl(productJson.getString(ProductAttribute.IMAGE));
            product.setBrand(productJson.getString(ProductAttribute.MANUFACTURER));
            product.setDescription(productJson.getString(ProductAttribute.DESCRIPTION));
            product.setThumbnailUrl(productJson.getString(ProductAttribute.THUMBNAIL));

            dstList.add(product);
        }

    }

    private String productAttributesToShow() {
        return ProductAttribute.IMAGE + "," +
                ProductAttribute.MANUFACTURER + "," +
                ProductAttribute.PRICE + "," +
                ProductAttribute.DESCRIPTION + "," +
                ProductAttribute.THUMBNAIL + "," +
                ProductAttribute.SKU + "," +
                ProductAttribute.NAME;
    }

    public enum Category {
        CELLPHONE("pcmcat209400050001");

        private String mPathId;

        Category(String pathId) {
            mPathId = pathId;
        }

        public String getPathId() {
            return mPathId;
        }
    }

    private static class ProductAttribute {
        public static final String IMAGE = "image";
        public static final String MANUFACTURER = "manufacturer";
        public static final String PRICE = "regularPrice";
        public static final String DESCRIPTION = "shortDescription";
        public static final String THUMBNAIL = "thumbnailImage";
        public static final String SKU = "sku";
        public static final String NAME = "name";
    }

}
