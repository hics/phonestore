package com.company.android.phonestore;

import android.support.v4.app.Fragment;

/**
 * Created by hics on 1/18/16.
 */
public class CatalogActivity extends SingleFragmentActivity {

    @Override
    protected Fragment createFragment() {
        return new CatalogFragment();
    }

}
