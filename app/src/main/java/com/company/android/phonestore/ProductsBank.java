package com.company.android.phonestore;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hics on 1/18/16.
 */
public class ProductsBank {

    private static ProductsBank sInstance;

    private List<Product> mProducts = new ArrayList<>();

    private ProductsBank() {
        // Empty
    }

    public static ProductsBank self() {
        if (sInstance == null) {
            sInstance = new ProductsBank();
        }
        return sInstance;
    }

    public Product findBySku(int sku) {
        for (Product p : mProducts) {
            if (p.getSku() == sku) {
                return p;
            }
        }
        return null;
    }

    public void add(List<Product> products) {
        mProducts.addAll(products);
    }

    public List<Product> getProducts() {
        return mProducts;
    }

    public void clear() {
        mProducts.clear();
    }

    public int count() {
        return mProducts.size();
    }

}
