package com.company.android.phonestore;

import android.os.Bundle;
import android.support.v4.app.Fragment;

/**
 * Created by hics on 1/18/16.
 */
public class ProductActivity extends SingleFragmentActivity{

    private int mProductSku;

    @Override
    protected Fragment createFragment() {
        return ProductFragment.newInstance(mProductSku);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        mProductSku = getIntent().getIntExtra(ProductFragment.PRODUCT_EXTRA, 0);
        super.onCreate(savedInstanceState);
    }
}
