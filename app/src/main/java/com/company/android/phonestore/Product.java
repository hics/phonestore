package com.company.android.phonestore;

/**
 * Created by hics on 1/14/16.
 */
public class Product {

    private int mSku;
    private float mPrice;
    private String mName;
    private String mBrand;
    private String mDescription;
    private String mImageUrl;
    private String mThumbnailUrl;

    public int getSku() {
        return mSku;
    }

    public void setSku(int sku) {
        mSku = sku;
    }

    public float getPrice() {
        return mPrice;
    }

    public void setPrice(float price) {
        mPrice = price;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public String getBrand() {
        return mBrand;
    }

    public void setBrand(String brand) {
        mBrand = brand;
    }

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(String description) {
        mDescription = description;
    }

    public String getImageUrl() {
        return mImageUrl;
    }

    public void setImageUrl(String imageUrl) {
        mImageUrl = imageUrl;
    }

    public String getThumbnailUrl() {
        return mThumbnailUrl;
    }

    public void setThumbnailUrl(String thumbnailUrl) {
        mThumbnailUrl = thumbnailUrl;
    }
}
