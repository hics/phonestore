# Catálogo de telefones da BestBuy. #

Tela principal com catálogo em grade e paginação automática.

Tela de detalhes com opção de Share da imagem do produto pelo Facebook 
(OBS.: aparentemente apenas funciona se o App do Facebook estiver instalado!)

Diálogo com imagem do produto ampliada. (clique na imagem para zoom)

---

Dependências:

* sdk v4.0.0 do Facebook

* biblioteca Picasso v2.5.2

---

Lembrete para configurar integração com Facebook:

1. Criar conta dev no site. 
2. Requisitar novo app e obter App Id. 
3. Trocar digitos finais do provider no Manifest pelo App Id. 
4. Também trocar string facebook_app_id para o App Id.

5. Gerar chave no terminal e adicionar na conta a string impressa na tela.

Para gerar chave:

```
#!
$ keytool -exportcert -alias androiddebugkey -keystore ~/.android/debug.keystore | openssl sha1 -binary | openssl base64

```

Para adicionar:

Configuration > New Platform > Campo "Hashes"